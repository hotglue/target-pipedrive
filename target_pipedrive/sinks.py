"""PipeDrive target sink class, which handles writing streams."""
import json
from datetime import datetime
from dateutil.parser import parse
from target_pipedrive.client import PipeDriveSink
from hotglue_models_crm.crm import (
    Deal,
    Company,
    Contact,
    User,
    Activity,
    Product,
    Note
)

def owner_lookup(instance, record):
    if record.get('owner_id') is not None:
        return record.get('owner_id')

    owner_id = None
    if record.get('owner_email'):
        url = "users"
        users = instance.request_api("GET", endpoint=url).json().get("data", [])
        for user in users:
            if user.get("email") == record.get("owner_email"):
                owner_id = user.get("id")
                break

    if owner_id is None and record.get('owner_name'):
        url = "/".join(["users", "find"])
        params = {
            "term":record.get("owner_name"),
            "exact_match":True,
        }
        owner = instance.request_api("GET", endpoint=url,params=params)
        owner = instance.extract_search_results(owner)
        if "id" in owner:
            owner_id = owner['id']

    return owner_id


def organization_lookup(instance, record):
    company_id = None
    if record.get('company_id') is not None:
        return record.get('company_id')

    if record.get("company_name") and not record.get("company_id"):
            url = "/".join(["organizations","search"])
            params = {
                "term":record.get("company_name"),
                "exact_match":True,
            }
            company = instance.request_api("GET", endpoint=url,params=params)
            company = instance.extract_search_results(company)
            if "id" in company:
                company_id = company['id']

    return company_id


class DealsSink(PipeDriveSink):
    endpoint = "deals"
    unified_schema = Deal
    name = Deal.Stream.name

    def preprocess_record(self, record: dict, context: dict) -> dict:
        if record.get("expected_close_date"):
            record["expected_close_date"] = parse(record["expected_close_date"])

        record = self.validate_input(record)

        mapping = {
            "title": record.get("title"),
            "id": record.get("id"),
            "value": record.get("monetary_amount"),
            "currency": record.get("currency"),
            "person_id": record.get("contact_id"),
            "pipeline_id": record.get("pipeline_id"),
            "stage_id": record.get("pipeline_stage_id"),
            "org_id": organization_lookup(self, record),
            "user_id": owner_lookup(self, record),
        }

        mapping = self.validate_output(mapping)

        if record.get("win_probability"):
            mapping.update({"probability": record.get("win_probability")})

        if record.get("currency"):
            mapping.update({"currency": record.get("currency")})

        if record.get("lost_reason"):
            mapping.update({"lost_reason": record.get("loss_reason")})

        if record.get('created_at'):
            if not isinstance(record.get('created_at'), datetime):
                record["created_at"] = parse(record["created_at"])

            mapping.update({"add_time": record["created_at"].isoformat()})

        if record.get('close_date'):
            if not isinstance(record.get('close_date'), datetime):
                record["close_date"] = parse(record["close_date"])

            mapping.update({"close_date": record["close_date"].isoformat()})

        if record.get('updated_at'):
            if not isinstance(record.get('updated_at'), datetime):
                record["updated_at"] = parse(record["updated_at"])
            mapping.update({"update_time": record["updated_at"].isoformat()})

        if record.get('expected_closed_date'):
            if not isinstance(record.get('expected_closed_date'), datetime):
                record["expected_closed_date"] = parse(record["expected_closed_date"])

            mapping.update({"expected_closed_date": record["expected_closed_date"]})

        return mapping


class NotesSink(PipeDriveSink):
    endpoint = "notes"
    unified_schema = Note
    name = Note.Stream.name

    def preprocess_record(self, record: dict, context: dict) -> dict:
        if record.get("content"):
            record.update({"content": record.get("content")})
        record = self.validate_input(record)

        if record.get("deal_name") and not record.get("deal_id"):
            url = "/".join(["deals","search"])
            params = {
                "term":record.get("deal_name"),
                "exact_match":True,
            }
            deal = self.request_api("GET", endpoint=url, params=params)
            deal = self.extract_search_results(deal)
            if "id" in deal:
                record['deal_id'] = deal['id']


        elif record.get("customer_name") and not record.get("customer_id"):
            url = "/".join(["persons", "search"])
            params = {
                "term":record.get("customer_name"),
                "exact_match":True,
            }
            person = self.request_api("GET", endpoint=url, params=params)
            person = self.extract_search_results(person)
            if "id" in person:
                record['customer_id'] = person['id']

        mapping = {
            "content": record.get("content"),
            "deal_id": record.get("deal_id"),
            "org_id": organization_lookup(self, record),
            "person_id": record.get("customer_id"),
        }

        return self.validate_output(mapping)


class OrganizationSink(PipeDriveSink):
    endpoint = "organizations"
    unified_schema = Company
    name = Company.Stream.name

    def preprocess_record(self, record: dict, context: dict) -> dict:
        record["addresses"] = json.loads(record.get("addresses", []))
        record["social_links"] = json.loads(record.get("social_links", []))
        record = self.validate_input(record)

        mapping = {
            "name": record.get("name"),
            "add_time": record.get("created_at"),
            "user_id": owner_lookup(self, record)
        }
        if record.get("id"):
            mapping["id"] = record.get("id")

        mapping = self.validate_output(mapping)

        return mapping


class PersonsSink(PipeDriveSink):
    endpoint = "persons"
    unified_schema = Contact
    name = Contact.Stream.name

    def preprocess_record(self, record: dict, context: dict) -> dict:
        record = self.validate_input(record)

        mapping = {}
        if record.get("id"):
            mapping["id"] = record.get("id")

        mapping["owner_id"] = owner_lookup(self, record)
        mapping["org_id"] = organization_lookup(self, record)
        mapping["name"] = record.get("name")

        if record.get("email"):
            mapping["email"] = record.get("email")

        mapping["add_time"] = record.get("created_at")
        mapping = self.validate_output(mapping)

        return mapping


class UsersSink(PipeDriveSink):
    endpoint = "users"
    unified_schema = User
    name = User.Stream.name

    def preprocess_record(self, record: dict, context: dict) -> dict:
        record = self.validate_input(record)
        mapping = {}
        if record.get("id"):
            mapping["id"] = record.get("id")

        mapping["email"] = record.get("email")

        if record.get("active"):
            mapping["active_flag"] = record.get("active")
        else:
            mapping["active_flag"] = True

        return self.validate_output(mapping)


class ActivitiesSink(PipeDriveSink):
    endpoint = "activities"
    unified_schema = Activity
    name = Activity.Stream.name

    def preprocess_record(self, record: dict, context: dict) -> dict:
        if record.get("location_address"):
            record["location_address"] = json.loads(record["location_address"])

        record = self.validate_input(record)
        mapping = {}

        if record.get("id"):
            mapping["id"] = record.get("id")

        mapping["type"] = record.get("type")
        mapping["note"] = record.get("note")
        mapping["subject"] = record.get("title")
        mapping["deal_id"] = record.get("deal_id")
        mapping["public_description"] = record.get("description")

        if record.get("location_address"):
            mapping["location"] = ""
            record["location_address"].pop("id")
            for key in ["line1", "line2", "line3", "city", "state", "postal_code", "country"]:
                if record["location_address"].get("key") is not None:
                    mapping["location"] = f"{mapping['location']} {record['location_address'][key]},"

        if record.get("company_id"):
            mapping["org_id"] = record.get("company_id")

        elif record.get("company_name") and not record.get("company_id"):
            url = "/".join(["organizations", "search"])
            params = {
                "term":record.get("company_name"),
                "exact_match":True,
            }
            company = self.request_api("GET", endpoint=url,params=params)
            company = self.extract_search_results(company)
            if "id" in company:
                mapping['org_id'] = company['id']

        return self.validate_output(mapping)


class ProductsSink(PipeDriveSink):
    endpoint = "products"
    unified_schema = Product
    name = Product.Stream.name

    def preprocess_record(self, record: dict, context: dict) -> dict:
        prices = record.get("prices", [])
        if isinstance(prices, str):
            prices = json.loads(prices)

        mapping = {
            "name": record.get("name"),
            "code": record.get("sku"),
            "description": record.get("description"),
            "unit": str(record.get("unit")),
            "tax": record.get("tax"),
            "active_flag": record.get("is_active", True),
            "visible_to": record.get("visibility", "3"),
            "selectable": True,
            "prices": [
                {
                    "currency": price.get("currency"),
                    "price": price.get("price"),
                    "cost": price.get("cost")
                } for price in prices
            ]
        }

        if record.get("id"):
            mapping["id"] = record.get("id")

        return self.validate_output(mapping)