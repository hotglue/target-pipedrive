"""PipeDrive target class."""

from singer_sdk.target_base import Target
from singer_sdk import typing as th
from singer_sdk.sinks import Sink
from typing import Type
from target_pipedrive.sinks import (
    DealsSink,
    NotesSink,
    OrganizationSink,
    PersonsSink,
    UsersSink,
    ActivitiesSink,
    ProductsSink
)

SINK_TYPES = [
    DealsSink,
    NotesSink,
    OrganizationSink,
    PersonsSink,
    UsersSink,
    ActivitiesSink,
    ProductsSink
]


class TargetPipeDrive(Target):
    """Sample target for PipeDrive."""
    def __init__(
        self,
        config=None,
        parse_env_config: bool = False,
        validate_config: bool = True,
    ) -> None:
        self.config_file = config[0]
        super().__init__(config, parse_env_config, validate_config)

    name = "target-pipedrive"
    config_jsonschema = th.PropertiesList(
        th.Property("client_id", th.StringType, required=True),
        th.Property("client_secret", th.StringType, required=True),
        th.Property("refresh_token", th.StringType, required=True),
        th.Property("account", th.StringType, default=True),
    ).to_dict()

    def get_sink_class(self, stream_name: str) -> Type[Sink]:
        """Get sink for a stream."""
        return next(
            (
                sink_class
                for sink_class in SINK_TYPES
                if sink_class.name.lower() == stream_name.lower()
            ),
            None,
        )

if __name__ == '__main__':
    TargetPipeDrive.cli()
