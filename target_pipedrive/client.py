"""PipeDrive target sink class, which handles writing streams."""

from singer_sdk.sinks import RecordSink
from target_pipedrive.rest import Rest
from typing import Any, Dict, List, Optional, Callable, cast
from singer_sdk.plugin_base import PluginBase
from target_pipedrive.auth import OAuth2Authenticator

class PipeDriveSink(RecordSink, Rest):
    def __init__(
        self,
        target: PluginBase,
        stream_name: str,
        schema: Dict,
        key_properties: Optional[List[str]],
    ) -> None:
        """Initialize target sink."""
        self._target = target
        super().__init__(target, stream_name, schema, key_properties)

    @property
    def authenticator(self) -> OAuth2Authenticator:
        """Return a new authenticator object."""
        return OAuth2Authenticator(self._target, "https://oauth.pipedrive.com/oauth/token")    

    @property
    def name(self):
        raise NotImplementedError

    @property
    def endpoint(self):
        raise NotImplementedError

    @property
    def unified_schema(self):
        raise NotImplementedError

    def url(self, endpoint=None):
        if not endpoint:
            endpoint = self.endpoint
        instance_url = self.config.get("api_domain")
        if instance_url is None:
           instance_url = f"https://{self.config.get('account')}.pipedrive.com/api/v1" 
        else:
            instance_url = f"{self.config.get('api_domain')}/api/v1" 


        if not instance_url:
            self.authenticator
            instance_url = self.authenticator.instance_url
        return f"{instance_url}/{endpoint}"

    def validate_input(self, record: dict):
        return self.unified_schema(**record).dict()

    def validate_output(self, mapping):
        payload = self.clean_payload(mapping)
        return payload

    def process_record(self, record: dict, context: dict) -> None:
        """Process the record."""   
        if record.get("id"):
            url = "/".join([self.endpoint,record["id"]])
            response = self.request_api(
                "PUT", endpoint=url, request_data=record
            )
        else:    
            response = self.request_api("POST", request_data=record)
        try:
            id = response.json().get("id")
            self.logger.info(f"{self.name} created with id: {id}")
        except:
            pass     

    def extract_search_results(self,response):
        data = response.json()['data']
        if data == None:
            return {}
        
        if "items" in data:
            if len(data['items'])>0:
                if "item" in data['items'][0]:
                    return data['items'][0]['item']       
        return {}

    def get_or_create_custom_fields_ids(self, fields, field_type):
        if field_type not in ["organizationFields", "personFields", "dealFields"]:
            return None
    
        fields_on_api = {}
        all_fields = self.request_api("GET", endpoint=field_type).json().get("data", [])
        for field in all_fields:
            if field["name"] in fields.keys():
                fields_on_api[field["name"]] = field["key"]
                fields.pop(field["name"])
        
        for field, field_instance_type in fields.items():
            field_id = self.request_api(
                "POST",
                endpoint=field_type,
                request_data={"name": field, "field_type": field_instance_type},
            ).json().get("data", {}).get("key", None)
            fields_on_api[field] = field_id

        return fields_on_api